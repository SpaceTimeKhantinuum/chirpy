import time
import numpy as np
from numba import jit, float64


@jit('float64(float64[:, :])', nopython=True)
def jit_sum2d(arr):
    M, N = arr.shape
    result = 0.0
    for i in range(M):
        for j in range(N):
            result += arr[i, j]
    return result


def sum2d(arr):
    M, N = arr.shape
    result = 0.0
    for i in range(M):
        for j in range(N):
            result += arr[i, j]
    return result

np.random.seed(1)
# arr = np.random.rand(10000, 10000).astype(np.float64)
arr = np.random.rand(1000, 1000).astype(np.float64)
# arr = np.random.rand(100, 100).astype(np.float64)

st = time.time()
print(sum2d(arr))
no_jit_duration = time.time() - st
print("no jit duration = {}".format(no_jit_duration))

st = time.time()
print(jit_sum2d(arr))
jit_duration = time.time() - st
print("jit duration = {}".format(jit_duration))

print("jit is {} times faster".format(no_jit_duration / jit_duration))
