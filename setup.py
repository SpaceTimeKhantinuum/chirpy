#!/usr/bin/env python

from distutils.core import setup
from setuptools import find_packages


setup(name='gw-chirpy',
      description='Gravitational Waveforms in python',
      author='Sebastian Khan',
      author_email='sebastian.khan@LIGO.org',
      packages=find_packages(),
      url='https://gitlab.com/SpaceTimeKhantinuum/chirpy'
     )
