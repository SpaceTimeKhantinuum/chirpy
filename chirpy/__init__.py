import importlib.metadata
__version__ = importlib.metadata.version("gw_chirpy")
